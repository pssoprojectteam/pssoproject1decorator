package gof.scrabler;

import gof.ConverterUtil;

import java.util.Collections;

public class Scrambler {
	
	final static int REQUIED_SIZE = 256;
	
	private byte[] kBox;
	
	private byte[] sBox;
	
	private int i = 0;
	
	private int j = 0;
	
	public Scrambler(String key) {
		setKey(key);
	}
	
	public void setKey(String key) {
		kBox = getFilledKboxArray(key);
		sBox = getInitializedSboxArray();
		scrambleArrayParameters(kBox, sBox);
	}
	
	public byte[] encrypt(byte[] toEncrypt) {
		byte[] keyStream = new byte[toEncrypt.length];
		for (int index = 0; index < toEncrypt.length; ++index) {
			i = (i + 1) % REQUIED_SIZE;
			j = (j + ConverterUtil.fromUnsignedByteToInt(sBox[i])) % REQUIED_SIZE;
			ConverterUtil.swapArrayVariables(sBox, i, j);
			keyStream[index] = sBox[(
				ConverterUtil.fromUnsignedByteToInt(sBox[i]) 
				+ ConverterUtil.fromUnsignedByteToInt(sBox[j])
			) % REQUIED_SIZE];
		}
		return ConverterUtil.xor(toEncrypt, keyStream);
	}
	
	private void scrambleArrayParameters(byte[] key, byte[] s) {
		for (int i = 0, j = 0; i < REQUIED_SIZE; ++i) {
			j = (j + ConverterUtil.fromUnsignedByteToInt(s[i]) 
					+ ConverterUtil.fromUnsignedByteToInt(key[i])
				) % REQUIED_SIZE;
			ConverterUtil.swapArrayVariables(s, i, j);
		}
	}
	
	private byte[] getInitializedSboxArray() {
		byte[] s = new byte[REQUIED_SIZE];
		for (int p = 0; p < REQUIED_SIZE; p++) {
			s[p] = (byte) p;
		}
		return s;
	}
	
	private byte[] getFilledKboxArray(String key) {
		int howManyRepeat = (int) Math.ceil((double)REQUIED_SIZE / key.length());
		
		byte[] out = new byte[REQUIED_SIZE];
		byte[] bytes = String.join("", Collections.nCopies(howManyRepeat, key)).getBytes();
		System.arraycopy(bytes, 0, out, 0, REQUIED_SIZE);
		return out;
	}
}
