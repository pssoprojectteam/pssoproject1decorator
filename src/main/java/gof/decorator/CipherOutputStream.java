package gof.decorator;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import gof.ConverterUtil;
import gof.scrabler.Scrambler;

public class CipherOutputStream extends FilterOutputStream {

	final private static String DEFAULT_KEY = "dEfAuLt_KeY";
	private Scrambler scrambler;
	private OutputStream outputStream;
	
	public CipherOutputStream(OutputStream outputStream) {
		this(outputStream, DEFAULT_KEY);
	}

	public CipherOutputStream(OutputStream outputStream, String key) {
		super(outputStream);
		this.outputStream = outputStream;
		scrambler = new Scrambler(key);
	}
	
	@Override
	public void write(byte[] inBytes) throws IOException {
		write(inBytes, 0, inBytes.length);
	}
	
	@Override
	public void write(int byteInInt) throws IOException {
		write(ConverterUtil.intToByteArray(byteInInt, 1));
	}
	
	@Override
	public void write(byte[] in, int offset, int length) throws IOException {
		byte[] offsetedIn = new byte[length];
		System.arraycopy(in, offset, offsetedIn, 0, length);
		
		byte[] encrypted = scrambler.encrypt(offsetedIn);
		outputStream.write(encrypted, 0, length); 
	}
}
