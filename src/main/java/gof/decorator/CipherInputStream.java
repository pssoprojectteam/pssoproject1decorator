package gof.decorator;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import gof.ConverterUtil;
import gof.scrabler.Scrambler;

public class CipherInputStream extends FilterInputStream {

	final private static String DEFAULT_KEY = "dEfAuLt_KeY";
	private Scrambler scrambler;
	private InputStream inputStream;

	public CipherInputStream(InputStream inputStream) {
		this(inputStream, DEFAULT_KEY);
	}
	
	public CipherInputStream(InputStream inputStream, String key) {
		super(inputStream);
		this.inputStream = inputStream;
		scrambler = new Scrambler(key);
	}

	@Override
	public int read() throws IOException {
		byte[] arrayWithOneByte = new byte[1];
		int result = read(arrayWithOneByte);
		if (result == -1){
			return -1;
		}
		return ConverterUtil.byteArrayToInt(arrayWithOneByte);
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] out, int offset, int outMaxSize) throws IOException {
		byte[] in = new byte[outMaxSize];
		int readedBytes = inputStream.read(in, offset, outMaxSize);
		if (readedBytes == -1) {
			return -1;
		}
		
		byte[] encrypted = scrambler.encrypt(in);
		System.arraycopy(encrypted, 0, out, 0, readedBytes); // truncate 
		return readedBytes;
	}
}
