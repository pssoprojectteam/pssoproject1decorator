package gof;

import java.nio.charset.StandardCharsets;

public final class ConverterUtil {

	private ConverterUtil() {
	}

	public static byte[] intToByteArray(int x, int n) {
	    byte[] bytes = new byte[n];
	    for (int i = 0; i < n; i++, x >>>= 8)
	        bytes[i] = (byte) (x & 0xFF);
	    return bytes;
	}
	
	public static int byteArrayToInt(byte[] x) {
	    int value = 0;
	    for(int i = 0; i < x.length; i++)
	        value += ((long) x[i] & 0xffL) << (8 * i);
	    return value;
	}

	public static String bytesToString(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}

	public static String offsetString(String str, int offset, int len) {
		return str.substring(offset, offset + len);
	}

	public static void swapArrayVariables(byte[] array, int index1, int index2) {
		byte tmp = array[index1];
		array[index1] = array[index2];
		array[index2] = tmp;
	}

	public static byte[] xor(byte[] a, byte[] b) {
		byte[] result = new byte[Math.min(a.length, b.length)];

		for (int i = 0; i < result.length; i++) {
			result[i] = (byte) (((int) a[i]) ^ ((int) b[i]));
		}

		return result;
	}
	
	public static int fromUnsignedByteToInt(byte b) {
	    return b & 0xFF;
	}
}